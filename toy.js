// JavaScript Document
var $window = $( window );
var $scene = $( '.scene' );
var rX = 0;
var rY = 0;

$( '.face' ).prop( 'draggable', false );

$window.on( 'mousedown', startDrag );
$window.on( 'mouseup', stopDrag );

function startDrag() {
	$window.on( 'mousemove', rotateScene )
}

function stopDrag() {
	$window.off( 'mousemove', rotateScene )
}

function rotateScene( event ) {
	rY += event.originalEvent.movementX / 10;
	rX -= event.originalEvent.movementY / 10;    
	$scene.css( 'transform', 'rotateX('+ rX +'deg) rotateY('+ rY +'deg)' );
}

var $snowman = $('.snowman');
$snowman.on ('mouseover', moveSnowman);
function moveSnowman(){
	var tx = Math.random() * 560 + 220;
	//$snowman.css( { 'transform' : 'translateX('+tx+'px)' } );
	var tz = Math.random() * 265 - 635;
	//$snowman.css( { 'transform' : 'translateZ('+tz+'px)' } );
	$snowman.css( { 'transform' : 'translate3d('+tx+'px,460px ,'+tz+'px)' } );
}

var $jumpButton = $('.J1');
$jumpButton.on ('click', jumpSnowman);
function jumpSnowman(){
	$('#boing')[0].pause();
	$('#boing')[0].currentTime = 0;
	$('#music')[0].pause();
	$('#music')[0].currentTime = 0;
	$('#yay')[0].play();
	$snowman.css( { 'animation' : 'jump 2s ease-in-out 2 ' } );
}

var $spinButton = $('.S1');
$spinButton.on ('click', spinSnowman);
function spinSnowman(){
	$('#yay')[0].pause();
	$('#yay')[0].currentTime = 0;
	$('#music')[0].pause();
	$('#music')[0].currentTime = 0;
	$('#boing')[0].play();
	$snowman.css( { 'animation' : 'spin 2s linear 2 ' } );
}

var $xmasButton = $('.X1');
$xmasButton.on ('click', xmasSnowman);
function xmasSnowman(){
	$('#boing')[0].pause();
	$('#boing')[0].currentTime = 0;
	$('#yay')[0].pause();
	$('#yay')[0].currentTime = 0;
	$('#music')[0].play();
	$snowman.css( { 'animation' : 'xmas 4s linear' } );
}
